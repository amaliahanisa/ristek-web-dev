from django.shortcuts import render
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from .models import Feedback
from .forms import FeedbackForm


# Create your views here.

def index(request):
    return render(request, 'index.html')

def profile(request):
    return render(request, 'profile.html')

def create_feedback(request):
    if request.method == "POST":
        form = FeedbackForm(request.POST or None)
        if form.is_valid():
            feedback = Feedback()
            feedback.title = form.cleaned_data['title']
            feedback.save()
            print(feedback)
        return redirect('/contact')
    else:
        form = FeedbackForm()
        feedback = Feedback.objects.all()
        print(feedback)
        response = {'feedback': feedback, 'title': 'List', 'form':form}
        return render(request, "contact.html", response)
