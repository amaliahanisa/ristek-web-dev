from django.urls import re_path
from . import views

#url for app
urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    # re_path(r'^contact/$', views.contact, name='contact'),
    re_path(r'^profile$', views.profile, name='profile'),
    re_path(r'^contact/$', views.create_feedback,  name="create_feeadback")
]
