from django import forms
from .models import Feedback

class FeedbackForm(forms.Form):
    title = forms.CharField(label = "", max_length = 30, widget = forms.TextInput(
        attrs = {'required':True,}
    ))