from django.db import models
from django.utils import timezone

# Create your models here.
class Feedback(models.Model):
    title = models.CharField(max_length=300)
    time = models.TimeField(default=timezone.localtime(timezone.now()))
    date = models.DateField(default=timezone.now)

    def __str__(self):
        return self.title
