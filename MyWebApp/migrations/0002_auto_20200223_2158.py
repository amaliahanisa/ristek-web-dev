# Generated by Django 2.1.1 on 2020-02-23 14:58

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('MyWebApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedback',
            name='time',
            field=models.TimeField(default=datetime.datetime(2020, 2, 23, 14, 58, 17, 627850, tzinfo=utc)),
        ),
    ]
